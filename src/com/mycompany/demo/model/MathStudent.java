/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.demo.model;

/**
 *
 * @author vqtuan
 */
public class MathStudent extends Student{

    private double mathA1;
    private double mathA2;
    
    public MathStudent() {}

    public void printMathStudent(){
        String text = showBasicInfo() + ", mathA1 : " + mathA1 + ", mathA2 : " + mathA2 ;
        System.out.println( "MATH INFO : " + text );
    }
    
    //---------------------------------------
    public double getMathA1() {
        return mathA1;
    }

    public void setMathA1(double mathA1) {
        this.mathA1 = mathA1;
    }

    public double getMathA2() {
        return mathA2;
    }

    public void setMathA2(double mathA2) {
        this.mathA2 = mathA2;
    }

    
    
    
    
}
