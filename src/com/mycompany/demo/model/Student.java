/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.demo.model;

/**
 * 
 * 
 * @author vqtuan
 */
//abstract : là lớp cha , lớp cơ bản. Không sử dụng trực tiếp lớp abstract
public abstract class Student {
    
    private String name;
    private int old;
    private double point;

    // Hàm khởi tạo 1: Constructor
    public Student() {}
    
    // Hàm khởi tạo 2: Constructor
    public Student( String name, int old, double point) {
        this.name = name;
        this.old = old;
        this.point = point;
    }
    
    public String showBasicInfo(){
        return "name : " + name + ", old : " + old + ", point : " + point ;
    }

    //----------------------------------------------------------
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getOld() {
        return old;
    }

    public void setOld(int old) {
        this.old = old;
    }

    public double getPoint() {
        return point;
    }

    public void setPoint(double point) {
        this.point = point;
    }

    
    
}
