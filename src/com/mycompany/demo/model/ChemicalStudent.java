/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.demo.model;

/**
 *
 * @author vqtuan
 */
public class ChemicalStudent extends Student{

    private double chemicalA1;
    private double chemicalA2;
    
    public ChemicalStudent() {}

    public void printChemicalStudent(){
        String text = showBasicInfo() + ", chemicalA1 : " + chemicalA1 + ", chemicalA2 : " + chemicalA2 ;
        System.out.println( "CHEMICAL INFO : " + text );
    }
    
    //------------------------------------------------------
    public double getChemicalA1() {
        return chemicalA1;
    }

    public void setChemicalA1(double chemicalA1) {
        this.chemicalA1 = chemicalA1;
    }

    public double getChemicalA2() {
        return chemicalA2;
    }

    public void setChemicalA2(double chemicalA2) {
        this.chemicalA2 = chemicalA2;
    }
    
    
}
