/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.demo.view;

import com.mycompany.demo.model.ChemicalStudent;
import com.mycompany.demo.model.MathStudent;

/**
 *
 * @author vqtuan
 */
public class RegisterForm {
    
    // Hàm để chạy chương trình
    public static void main(String[] args){
        
        // B1: Khởi tạo các student
        MathStudent mathStudent1 = new MathStudent();
        mathStudent1.setName("Nguyen Van A");
        mathStudent1.setOld(19);
        mathStudent1.setPoint(27);
        mathStudent1.setMathA1(8);
        mathStudent1.setMathA2(6);
        
        MathStudent mathStudent2 = new MathStudent();
        mathStudent2.setName("Nguyen Van B");
        mathStudent2.setOld(18);
        mathStudent2.setPoint(28);
        mathStudent2.setMathA1(5);
        mathStudent2.setMathA2(6);
        
        ChemicalStudent chemicalStudent1 = new ChemicalStudent();
        chemicalStudent1.setName("Nguyen Thi A");
        chemicalStudent1.setOld(19);
        chemicalStudent1.setPoint(27);
        chemicalStudent1.setChemicalA1(5);
        chemicalStudent1.setChemicalA2(7);
        
        ChemicalStudent chemicalStudent2 = new ChemicalStudent();
        chemicalStudent2.setName("Nguyen Thi B");
        chemicalStudent2.setOld(19);
        chemicalStudent2.setPoint(27);
        chemicalStudent2.setChemicalA1(10);
        chemicalStudent2.setChemicalA2(7);
        
        
        // B2: In thông tin các student
        
        mathStudent1.printMathStudent() ;
        mathStudent2.printMathStudent() ;
        
        chemicalStudent1.printChemicalStudent();
        chemicalStudent2.printChemicalStudent();
        
    }
    
}
